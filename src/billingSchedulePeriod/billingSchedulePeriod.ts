/* start to define the BillingSchedulePeriod here */
/* end BillingSchedulePeriod here */

export interface SplitEvent {
  id: string;
  startDate: Date;
  endDate: Date;
  amount: number;
}
