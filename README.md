# Project: Billing Scheulder

## Getting Started

---

### Prerequisite

---

- NPM
- NodeJS

#### NPM & NodeJs

---

For NPM and NodeJs, it is highly recommended that you use a environment manager.
The environment manager will prevent pollution of your local system.

##### Quickest Way

This can take some time to setup and understand how to use. For the quickest
start, navigate to the [Download Page of Nodejs](https://nodejs.org/en/download/),
download the correct package for your system, and install.

##### Industry Standard Way

###### Linux/macOs

I highly recommend [NVM](https://github.com/nvm-sh/nvm).
Read about the details on the NVM project page.

###### Windows

Setting up development for Windows is a little bit more complicated. There are
three (3) pieces of technology you will most likely need:

1. terminal
2. SSH
3. git

For the terminal, I would recommend zsh or bash. Here is a tutorial on [setting
up zsh on your Windows Machine](https://dev.to/zinox9/installing-zsh-on-windows-37em).

On top of that, you will likely need to setup [SSH](https://docs.microsoft.com/en-us/windows/terminal/tutorials/ssh)
and [git](https://git-scm.com/download/win). Please consult the documentation
I've linked above to set those pieces of technology up.

After that, the Environment Manager I recommend for Windows is [nvm-windows](https://github.com/coreybutler/nvm-windows).

### Starting Development

Validate that you have Node and NPM:

```bash
node -v
```

```bash
npm -v
```

### Development

---

#### Install

Running this command will install all the dependencies for the backend project.

```bash
npm install
```

#### Build

Running this command will build a production ready version of the project.

```bash
npm run build
```

#### Lint

Running this command will run ESLint on the Persistence Service project.

```bash
npm run lint
```

#### Testing

Running this command will run all the tests

```bash
npm run test
```

Running this command will run the tests in watch mode (the tests will re-run on save)

```bash
npm run test -- --watch
```
